package Lesson3;

//2)10luq say sisteminde daxil edilmish ededi 2lik koda
//cheviren program yazin.
//4)10lug say sisteminde daxil edilmish ededi 8liye cevirin
//5)10lug say sisteminde daxil edilmish ededi 16liga cevirin

import java.util.Scanner;

public class DecimalToBinary {
    public static void main(String[] args) {
        Scanner number_scan = new Scanner(System.in);
        System.out.print("10-luq say sistemində bir ədəd daxil edin: ");
        int number = number_scan.nextInt();
        System.out.println("Ikilik qarşılığı: " + Integer.toBinaryString(number));
        System.out.println("Səkkizlik say sistemində qarşılığı: " + Integer.toOctalString(number));
        System.out.println("Onaltılıq say sistemində qarşılığı: " + Integer.toHexString(number));
    }
}
