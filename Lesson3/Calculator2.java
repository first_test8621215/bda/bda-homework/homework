package Lesson3;
//15)Scannerden iki eded alin ve +,-,*,/, bu sim vollardan hansi gelse o emelyati
//aparsin(kalkulator);
import java.util.Arrays;
import java.util.Scanner;

public class Calculator2 {
    public static void main(String[] args) {
        Scanner operation_scan = new Scanner(System.in);
        String operation_str = operation_scan.nextLine();
        String[] arithmetic_ops = {"+", "-", "*", "/"};

        for (int i = 0; i < operation_str.length(); i++) {
            for (String arithmetic_op : arithmetic_ops) {
                if (String.valueOf(operation_str.charAt(i)).equals(arithmetic_op)) {
                    String[] operation_arr = operation_str.split("\\" + arithmetic_op);
                    float result = Integer.parseInt(operation_arr[0]);
                    String[] operation_arr_last = Arrays.copyOfRange(operation_arr, 1, operation_arr.length);

                    if (arithmetic_op.equals("+")) {
                        for (String operation_arr_element : operation_arr_last) {
                            result += Integer.parseInt(operation_arr_element);
                        }
                    } else if (arithmetic_op.equals("-")) {
                        for (String operation_arr_element : operation_arr_last) {
                            result -= Integer.parseInt(operation_arr_element);
                        }
                    } else if (arithmetic_op.equals("*")) {
                        for (String operation_arr_element : operation_arr_last) {
                            result *= Integer.parseInt(operation_arr_element);
                        }
                    } else if (arithmetic_op.equals("/")) {
                        for (String operation_arr_element : operation_arr_last) {
                            result /= Integer.parseInt(operation_arr_element);
                        }
                    } else {
                        System.out.println("Unresolved Operand.");
                    }
                    System.out.println(result);
                }
            }
        }
    }
}


