package Lesson3;
//11) Verilmish ededin tek ve ya cut olmagini tapin

import java.util.Scanner;

public class OddAndEven {
    public static void main(String[] args) {
        Scanner scan_num  = new Scanner(System.in);
        System.out.println("Ədəd daxil edin: ");
        int num = scan_num.nextInt();

        if (num == 0){
            System.out.println("Sıfırdır");
        } else if (num % 2 == 0) {
            System.out.println("Cütdür");
        }else{
            System.out.println("Təkdir");
        }

    }
}
