package Lesson3;
//29. Method bir String ve bir int qebul edir ve hemin String-in hemin
//index-inde olan simvolunu qaytarsin.
//30. Method 2 String qebul edir ve onlarin beraber olub olmadigini
//return edir true ve ya false
//31.Method String s, char c, int count qebul edir. S-i count qeder c ile
//birleshdirir ve geriye return edir
//32)String a, String b, String c. A ve b-
//nin ichinde c varsa onda true eks halda false
//33.method String a, int begin, int end daxil edilir. A-nin begin ve
//end arasini alt alta chap edir.

public class İndexOfString {
    public static void main(String[] args) {
        System.out.println(IndexOfString("12345",2));
        System.out.println(IfStringsAreSame("1234","123"));
        System.out.println(foo("Soz", 'c', 5));
        System.out.println(abc("abcd","bcda","bc"));
        BeginToEndString("Hello World", 3,7);
    }
    public static char IndexOfString(String StringValue,int Index){
        return StringValue.charAt(Index);
    }

    public static boolean IfStringsAreSame(String FirstString,String SecondString){
        return FirstString.equals(SecondString);
    }
    public static String foo(String s, char c, int count){
        String return_value = "";
        return_value = new String(new char[count]).replace('\0', c);
        return s + return_value;
    }
    public static boolean abc(String a, String b, String c){
        return a.contains(c) & b.contains(c);
    }
    public static void BeginToEndString(String a, int begin, int end){
        for(; begin < end; begin++){
            System.out.println(a.charAt(begin));
        }

    }
}
