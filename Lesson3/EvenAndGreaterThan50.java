package Lesson3;
//16). Scannerden eded girin ve bu eded hem cut eded hem 50 den boyuk olsa
//"Ugurlu emelyat" bu ikisinden birini odemirse "UGURSUZ emelyat" yazsin;
//17) Scannerden eded girin eyer bu eden ya cutdurse yada 100den boyukdurse
//"UGURLU"(her ikisinden biri uygundursa). deyilse "UGURSUZ" yazsin

import java.util.Scanner;

public class EvenAndGreaterThan50 {
    public static void main(String[] args) {
        Scanner number_scan = new Scanner(System.in);
        Scanner number_scan2 = new Scanner(System.in);
        System.out.print("Birinci ədəd: ");
        int number = number_scan.nextInt();

        if (number % 2 ==0 & number > 50){
            System.out.println("Uğurlu Əməliyyat");
        }else{
            System.out.println("Uğursuz Əməliyyat");
        }

        System.out.print("Ikinci ədəd: ");
        int number2 = number_scan2.nextInt();
        if (number2 % 2 ==0 || number2 > 100){
            System.out.println("Uğurlu");
        }else{
            System.out.println("Uğursuz");
        }
    }
}
