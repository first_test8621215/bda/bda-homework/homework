package Lesson3;

//6) Boolean tip daxil edin eyer true olsa bu ededleri bir
//birine vurun(*) yox eyer false olsa bu ededleri toplayin

//8) Boolean tipinde deyer gotrun eyer boolean true olsa
//bashqa yazi deyilse bashqa bir sout verersiz.
import java.util.ArrayList;
import java.util.Scanner;

public class BooleanInput {
    public static void main(String[] args) {
        int a=5;
        int b=3;

        Scanner scan = new Scanner(System.in);
        System.out.print("Boolean qiymət daxil edin: ");
        String answer = scan.nextLine().toLowerCase();
        ArrayList<String> booleans = new ArrayList<String>(){{
            add("true");
            add("false");
        }};

        if (booleans.contains(answer)){
            if (answer.equals("true")){
                System.out.println(a*b);
            }else{
                System.out.println(a+b);
            }
        }else{
            System.out.println("Düzgün boolean qiymət daxil edilmədi.");
        }
    }
}
