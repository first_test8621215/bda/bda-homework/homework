package Lesson3;

//10) Switch Case-den istf ederek Heftenin gunlerini ve aylari chixartmaq.


import java.util.Calendar;
import java.util.TimeZone;

//10) Switch Case-den istf ederek Heftenin gunlerini ve aylari chixartmaq.
public class CalendarFunctions {
    public static void main(String[] args) {
        Calendar mycalendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Baku"));
        int dayOfWeek = mycalendar.get(Calendar.DAY_OF_WEEK);
        int month = mycalendar.get(Calendar.MONTH);

        String dayOfWeek_str = switch (dayOfWeek) {
            case 1 -> "Monday";
            case 2 -> "Tuesday";
            case 3 -> "Wednesday";
            case 4 -> "Thursday";
            case 5 -> "Friday";
            case 6 -> "Saturday";
            case 7 -> "Sunday";
            default -> "";
        };

        String month_str = switch (month) {
          case 1 -> "January";
          case 2 -> "February";
          case 3 -> "Mart";
          case 4 -> "April";
          case 5 -> "May";
          case 6 -> "June";
          case 7 -> "July";
          case 8 -> "August";
          case 9 -> "September";
          case 10 -> "October";
          case 11 -> "November";
          case 12 -> "December";
          default -> "";

        };

        System.out.println(dayOfWeek_str);
        System.out.println(month_str);

    }
}
