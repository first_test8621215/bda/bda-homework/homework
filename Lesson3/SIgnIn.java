package Lesson3;

//34)Bizdən istifadəçi adını və şifrə tələb edən proqram
//tərtib edin. (Proqramın içində dəyərləri static olaraq təyin
//edin.) Daha sonra username səhv yazıldıqda username
//yanlışdır. Şifrəni səhv yazdıqda şifrə yanlışdır desin. Və
//şifrəni 3-cü dəfədən artıq yanlış yığarsa sistemdən çıxsın.
//Əks halda hər biri doğrudursa “Xoş gəldiniz, ‘username’!”

import java.util.Scanner;

public class SIgnIn {
    public static void main(String[] args) {
        String username = "admin";
        String password = "admin123";

        Scanner usernameInputScan = new Scanner(System.in);
        Scanner passwordInputScan = new Scanner(System.in);



        int attemptCount = 0;
        do {
            System.out.print("Username: ");
            String loginUser = usernameInputScan.nextLine();
            System.out.print("Password: ");
            String loginPassword = passwordInputScan.nextLine();

            if (loginUser.equals(username) & loginPassword.equals(password)) {
                System.out.println("Welcome Admin");
            } else if (loginUser.equals(username) & !loginPassword.equals(password)) {
                System.out.println("Password is incorrect");
            } else if (loginPassword.equals(password) & !loginUser.equals(username)) {
                System.out.println("Username is incorrect");
            } else {
                System.out.println("None of them is true");
            }
            attemptCount++;
        } while (attemptCount < 3);
    }
}
