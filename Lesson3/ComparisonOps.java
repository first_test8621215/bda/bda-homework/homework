package Lesson3;

import java.util.Scanner;

//19). Scanner eded daxil edin eyer gelen eded hem 100den boyuk hemde
//musbetdirse uzerine 15 ededi gelin
//20)Scannerden eded daxil edin eyer gelen eded ya musbet yada 50den boyukdurse
//uzerine 10 ededi gelin
public class ComparisonOps {
    public static void main(String[] args) {
        Scanner number_scan = new Scanner(System.in);
        Scanner number_scan2 = new Scanner(System.in);
        System.out.print("Birinci ədəd: ");
        int number = number_scan.nextInt();
        if (number>100){
            System.out.println(number+15);
        }else{
            System.out.println("100-dən kiçikdir.");
        }

        System.out.print("Ikinci ədəd: ");
        int number2 = number_scan2.nextInt();
        if (number2 > 50 || number2 > 0){
            System.out.println(number2+10);
        }else{
            System.out.println("Şərtlər ödənmədi.");
        }

    }
}
