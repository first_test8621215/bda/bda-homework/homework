package Lesson3;

//7)Biz deyirik ki switch if-den daha suretlidir. Bu ne ucun beledir? Bunu kod numensi
//yazaraq izah edin
public class SwitchComparedToIf {
    public static void main(String[] args) throws InterruptedException{
        int a=15;
        long start_switch = System.currentTimeMillis();
        switch (a) {
            case 1:
                System.out.println("1");
                break;
            case 2:
                System.out.println("2");
                break;
            case 3:
                System.out.println("3");
                break;
            case 4:
                System.out.println("4");
                break;
            case 5:
                System.out.println("5");
                break;
            case 6:
                System.out.println("6");
                break;
            case 7:
                System.out.println("7");
                break;
            case 8:
                System.out.println("8");
                break;
            case 9:
                System.out.println("9");
                break;
            case 10:
                System.out.println("10");
                break;
            case 11:
                System.out.println("11");
                break;
            case 12:
                System.out.println("12");
                break;
            case 13:
                System.out.println("13");
                break;
            case 14:
                System.out.println("14");
                break;
            case 15:
                System.out.println("15");
                break;
        }

        System.out.println("Switch: " + (System.currentTimeMillis()-start_switch));

        long start_if = System.currentTimeMillis();
        if (a==1){
            System.out.println("1");
        }
        if(a==2){
            System.out.println("2");
        }
        if(a==3){
            System.out.println("3");
        }
        if(a==4){
            System.out.println("4");
        }
        if(a==5){
            System.out.println("5");
        }
        if (a==6){
            System.out.println("6");
        }
        if(a==7){
            System.out.println("7");
        }
        if(a==8){
            System.out.println("8");
        }
        if(a==9){
            System.out.println("9");
        }
        if(a==10){
            System.out.println("10");
        }
        if (a==11){
            System.out.println("11");
        }
        if(a==12){
            System.out.println("12");
        }
        if(a==13){
            System.out.println("13");
        }
        if(a==14){
            System.out.println("14");
        }
        if(a==15){
            System.out.println("15");
        }

        System.out.println("If: " + (System.currentTimeMillis()-start_if));
    }
}
