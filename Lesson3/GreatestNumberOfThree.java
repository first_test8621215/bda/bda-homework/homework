package Lesson3;

//25)Scanner ile mushteriden 3 reqem isteyin ve en boyuk reqem hansidirsa
//onu chap edin

import java.util.Arrays;
import java.util.Scanner;

public class GreatestNumberOfThree {
    public static void main(String[] args) {
        Scanner num1_scan = new Scanner(System.in);
        Scanner num2_scan = new Scanner(System.in);
        Scanner num3_scan = new Scanner(System.in);

        System.out.print("Birinci ədədi daxil edin: "); int num1 = num1_scan.nextInt();
        System.out.print("Ikinci ədədi daxil edin: "); int num2 = num2_scan.nextInt();
        System.out.print("Üçüncü ədədi daxil edin: "); int num3 = num3_scan.nextInt();

        int[] numArray = {num1,num2,num3};
        int maxNum = Arrays.stream(numArray).max().orElse(0);
        System.out.println("Ən böyük rəqəm: " + maxNum);

    }
}
