package Lesson3;

//1)Calculator appinin switch və methodlar ilə yazılması.
//22)Scanner-dən istifadə edərək calculator appi yazın
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {
        Scanner count = new Scanner(System.in);
        System.out.print("How many values would you like to input?: ");
        int count_value = Integer.parseInt(count.nextLine());

        Scanner operation = new Scanner(System.in);
        System.out.print("""
                Choose the operation:
                1) +
                2) -
                3) *
                4) /
                5) Exit       
                """);
        int operation_value = operation.nextInt();

        Scanner values_scan = new Scanner(System.in);
        List<Integer> values = new ArrayList<Integer>();
        for (int i = 0; i < count_value; i++) {
            System.out.print("Your value: ");
            values.add(values_scan.nextInt());
        }

        float result = values.get(0);


        switch (operation_value) {
            case (1):
                for (int i = 1; i < count_value; i++) {
                    result = result + (float) values.get(i);
                }
                break;
            case (2):
                for (int i = 1; i < count_value; i++) {
                    result = result - (float) values.get(i);
                }
                break;
            case (3):
                for (int i = 1; i < count_value; i++) {
                    result = result * (float) values.get(i);
                }
                break;
            case (4):
                for (int i = 1; i < count_value; i++) {
                    result = result / (float) values.get(i);
                }
                break;
            case (5):
                System.exit(0);
        }

        System.out.println(result);


    }

}


