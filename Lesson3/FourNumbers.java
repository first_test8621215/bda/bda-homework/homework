package Lesson3;
//27. Methoda 4 reqem daxil edilir eger bu reqemlerden her hansisa
//2-si bir birine beraberdirse geriye true qaytarsin

//28. Methoda 4 reqem daxil edilir.a,b,c,d eger ededler artan
//ardicilliqla daxil edilibse geriye true qaytarsin eks halda false

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FourNumbers {
    public static void main(String[] args) {
        ArrayList<Integer> arr_list = new ArrayList<>();
        arr_list.add(99);
        arr_list.add(29);
        arr_list.add(99);
        arr_list.add(115);

        String result = FindRepeatedNumbers(arr_list);
        System.out.println(result);

        System.out.println(IfValuesAccordinglyAdded(arr_list));

    }

    public static String FindRepeatedNumbers(ArrayList<Integer> num_args) {
        String printMessage = "";
        ArrayList<Integer> checkedNumbers = new ArrayList<>();
        for (int index=0; index<num_args.size(); index++) {
            int num = num_args.get(index);

            int ifSameNumber = 0;
            for(int checkednumber : checkedNumbers){
                if (num == checkednumber){
                    ifSameNumber = 1;
                    break;
                }
            }

            if (ifSameNumber != 0){
                continue;
            }

            List<Integer> tempArr = new ArrayList<>();
            tempArr = num_args.subList(index+1,num_args.size());
            int i = 1;
            for (int number : tempArr){
                if (num == number) {
                    i += 1;
                }
            }

            if (i > 1){
                printMessage += "The number " + num + " have been repeated " + i + " times!\n";
                checkedNumbers.add(num);
            }
        }

        if (printMessage.length() > 0){
            return printMessage;
        }else{
            return "No value has been repeated!";
        }
    }

    public static boolean IfValuesAccordinglyAdded(ArrayList<Integer> num_args){
        boolean returnValue = true;

        for (int i=0; i<num_args.size(); i++){
            List<Integer> tempArr = new ArrayList<>();
            tempArr = num_args.subList(i+1,num_args.size());

            for (int number : tempArr){
                if (num_args.get(i) > number) {
                    returnValue = false;
                    break;
                }
            }
        }
        return returnValue;
    }
}
