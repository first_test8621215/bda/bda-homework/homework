package Lesson16;

import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner = new Scanner(System.in);
        ArrayList<ArrayList<Integer>> numbers = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer> tempList;

        int n = scanner.nextInt();
        scanner.nextLine(); // Consume the remaining newline character

        for (int i = 0; i < n; i++) {
            String dLine = scanner.nextLine();
            int d = Integer.parseInt(dLine.split(" ")[0]);
            String[] tempNumbers = dLine.split(" ");
            tempList = new ArrayList<Integer>();
            for (int i2 = 1; i2 < d + 1; i2++) {
                tempList.add(Integer.parseInt(tempNumbers[i2]));
            }
            numbers.add(tempList);
        }

        int q = scanner.nextInt();
        scanner.nextLine(); // Consume the remaining newline character

        ArrayList<ArrayList<Integer>> arrayOfPair = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer> tempList2;
        for (int i = 0; i < q; i++) {
            tempList2 = new ArrayList<Integer>(q);
            String xy = scanner.nextLine();
            int x = Integer.valueOf(xy.split(" ")[0]);
            int y = Integer.valueOf(xy.split(" ")[1]);
            tempList2.add(x);
            tempList2.add(y);
            arrayOfPair.add(tempList2);
        }

        for (int i = 0; i < arrayOfPair.size(); i++) {
            try {
                System.out.println(numbers.get(arrayOfPair.get(i).get(0) - 1).get(arrayOfPair.get(i).get(1) - 1));
            } catch (IndexOutOfBoundsException msg) {
                System.out.println("ERROR!");
            }
        }
    }
}