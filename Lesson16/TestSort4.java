package Lesson16;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

class Student{
    public String name;
    public int age;
    public Student(String name){
        this.name=name;
        this.age = age;
    }
    public int compareTo(Student person) {
        return name.compareTo(person.name);
    }
}
public class TestSort4 {
    public static void main(String[] args){
        ArrayList<Student> al = new ArrayList<Student>();
        al.add(new Student("Viru"));
        al.add(new Student("Saurav"));
        al.add(new Student("Mukesh"));
        al.add(new Student("Tahir"));
        ArrayList<String> al2 = new ArrayList<String>();
        al2.add("SUMQAYIT");
        al2.add("BAKI");
        Collections.sort(al2);

        for(String s: al2){
            System.out.println(s);
        }
    }
}
