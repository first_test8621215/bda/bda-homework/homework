package Lesson6;

//Employee classı yaradın. Və içərisində (id, full-name, company, department, position və salary) olsun.
// Daha sonra bir method yaradın, məlumatları içərisinə parametr olaraq göndərək və bizə console da məlumatları çap etsin.
public class Employee {
    public int id;
    public String fullName;
    public String company;
    public String department;
    public String position;
    public int salary;

    public Employee(int id, String fullName, String company, String department, String position, int salary) {
        this.id = id;
        this.fullName = fullName;
        this.company = company;
        this.department = department;
        this.position = position;
        this.salary = salary;
    }

    public String ShowEverything() {
        return "ID: " + this.id + "\nFull Name: " + this.fullName + "\nCompany: " + this.company + "\nDepartment: " + this.position + "\nSalary: " + this.salary;
    }
}
