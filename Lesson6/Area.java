package Lesson6;
//Area adlı bir class yaradaq. Və biz onun içində olan bir methoda length və height göndərək. Bizə həm sahəni, həm də perimetri hesablayıb qaytarsın.
public class Area {
    public static String AreaAndPerimeterOfRectangle(double length, double height){
        double area = length * height;
        double perimeter = (length+height)*2;

        return String.format("Area: %s\nPerimeter: %s", String.valueOf(area), String.valueOf(perimeter));
    }
}
