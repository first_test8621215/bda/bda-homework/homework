package Lesson6;

////Dairənin sahəsini və perimetrini qaytaran class yaradın. (Circle)
public class Circle {
    public static double calculateCircleArea(double radius) {
        return Math.PI * Math.pow(radius, 2);
    }
}
