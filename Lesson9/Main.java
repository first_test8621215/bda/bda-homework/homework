package Lesson9;

class Animal {
    public int age;

    public void showAge() {
        System.out.println(this.age);
    }
}

    class Dog extends Animal {
        public Dog() {
            this.age = 6;
        }

        @Override
        public void showAge() {
            System.out.println(this.age);
        }

        public void run() {
            System.out.println("Dog is running");
        }
    }

    class Cow extends Animal {
        public Cow() {
            this.age = 10;
        }
    }

    public class Main {
        public static void main(String[] args) {
            Dog dog = new Dog();
            dog.showAge();
            dog.run();

            Cow cow = new Cow();
            cow.showAge();
        }
    }