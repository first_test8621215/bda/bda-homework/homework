package Lesson19;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Translation {
    static Scanner intScan = new Scanner(System.in);
    static Scanner stringScan = new Scanner(System.in);
    static List<String> lines;

    public static void main(String[] args) throws IOException {
        while (true) {
            intro();
            int choice = intScan.nextInt();

            switch (choice) {
                case 1:
                    System.out.print("Neçə söz soruşmaq istəyirsiniz ?: ");
                    int wordNumberForEnglish = intScan.nextInt();

                    for (int i = 0; i < wordNumberForEnglish; i++) {
                        System.out.print("Azərbaycan dilində söz: ");
                        String word = stringScan.nextLine();
                        System.out.println(AzerbaijaniIntoEnglish(word));
                    }
                    break;
                case 2:
                    System.out.print("Neçə söz soruşmaq istəyirsiniz ?: ");
                    int wordNumberForAzerbaijani = intScan.nextInt();

                    for (int i = 0; i < wordNumberForAzerbaijani; i++) {
                        System.out.print("Word in English: ");
                        String word = stringScan.nextLine();
                        System.out.println(EnglishIntoAzerbaijani(word));
                    }
                    break;
                case 3:
                    showWordlist();
                    break;
                case 4:
                    readingFile();
                    System.out.println(lines.size());
                    break;
                case 5:
                    System.out.print("Neçə söz daxil etmək istəyirsiniz ?: ");
                    int wordCount = intScan.nextInt();
                    String resultOfAddingWords = addingNewWords(wordCount);
                    System.out.println(resultOfAddingWords);
                    break;
                case 6:
                    System.exit(0);
            }
        }
    }


    static void intro() {
        System.out.print("1. Azerbaijan to English\n" +
                "2. English to Azerbaijan\n" +
                "3. Show wordlist\n" +
                "4. Show words count\n" +
                "5. add new words\n" +
                "6. exit\n" +
                "Seçimin: ");
    }

    static String addingNewWords(int wordcount) throws IOException {
        File dictionary = new File("dictionary.txt");
        FileWriter dictWriter = new FileWriter(dictionary, true);
        for (int i = 1; i < wordcount + 1; i++) {
            System.out.println("Word " + i + " :");
            System.out.print("in English: ");
            String wordInEnglish = stringScan.nextLine();
            System.out.print("Azərbaycan dilində: ");
            String wordInAzerbaijanian = stringScan.nextLine();

            dictWriter.write(wordInAzerbaijanian + ":" + wordInEnglish + "\n");
            dictWriter.flush();
        }
        dictWriter.close();
        return "Bütün sözlər əlavə olundu!";
    }

    static void readingFile() throws IOException {
        String filePath = "dictionary.txt";
        Path path = Paths.get(filePath);
        lines = Files.readAllLines(path);
    }

    static void showWordlist() throws IOException {
        readingFile();
        for (String line : lines) {
            System.out.print(line.split(":")[0] + " <-> " + line.split(":")[1] + "\n");
        }
    }

    static String EnglishIntoAzerbaijani(String word) throws IOException {
        readingFile();
        for (String line : lines) {
            if (line.split(":")[1].equals(word)) {
                return line.split(":")[0];
            }
        }
        return "Söz tapılmadı !";
    }

    static String AzerbaijaniIntoEnglish(String word) throws IOException {
        readingFile();
        for (String line : lines) {
            if (line.split(":")[0].equals(word)) {
                return line.split(":")[1];
            }
        }
        return "Söz tapılmadı !";
    }
}