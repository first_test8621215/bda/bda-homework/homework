package Lesson19;

import java.io.*;

public class FileIO {
    public static void main(String[] args) throws IOException {
        File ft = new File("myfile2.txt");
        Reader ftWr = new FileReader(ft);
        System.out.println(ftWr.read());
        ftWr.close();
    }
}
