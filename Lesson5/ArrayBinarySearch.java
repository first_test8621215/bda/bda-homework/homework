package Lesson5;

import java.util.Arrays;

//Arraysin binarysearch methodundan istifade ederek axtardiginiz deyerin hansi indexed yerleshdiyini tapin.
public class ArrayBinarySearch {
    public static void main(String[] args) {
        String[] words = {"ab","ac","ad","ae","af"};
        String key = "af";
        System.out.println(Arrays.binarySearch(words,key));
    }
}
