package Lesson5;

//5.Daxil edilmiş 2 ölçülü massivin bütün 5-ə bölünən ədədlərini çap edən proqram tərtib edin.
//6.Daxil edilmiş 2 ölçülü massivin bütün elementlərinin cəmini hesablayan proqram yazın.
public class _2DArray {
    public static void main(String[] args) {
        int[][] twoDArray = {
                {1, 2, 3, 14},
                {4, 5, 6, 11},
                {7, 8, 9, 10}
        };

        int rows = twoDArray.length;
        int cols = twoDArray[0].length;
        int sum = 0;

        for(int i=0; i< rows ; i++){
            for(int i2=0; i2<cols; i2++){
                if (twoDArray[i][i2] % 5 == 0){
                    System.out.println(twoDArray[i][i2]);
                }
                sum += twoDArray[i][i2];
            }
        }
        System.out.println("Total Sum: " + sum);
    }

}
