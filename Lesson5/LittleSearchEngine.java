package Lesson5;

import java.util.Scanner;

//4.Demek, kichik bir axtarish sistemi quracayiq. String tipinde olan massivde bir chox adlar yer alsin. Ve daha sonra consoleda bizden input istenilsin. Meselen:
//String[] users = {"Abbas", "Leman", "Xedice", "Ilyas", "Nurlan", "Nihat", "Elchin", "Murad", "Mirhesen", "Emin", "Farid", "Terane"}; - deye bir massivimiz var.
//Men consoledan ‘tera’ – daxil ederken proqram anlasin ki, men teraneni axtariram. Ve hemin adi tamamlayaraq yaninda indexide olmaqla chap etsin. Yox eger uygun gelen bir chox ad varsa hamisini  chap etsin. Eks halda ise bele bir user yoxdur desin.
public class LittleSearchEngine {
    public static void main(String[] args) {
        Scanner searchingValueScan = new Scanner(System.in);
        System.out.print("Search: ");
        String searchingValue = searchingValueScan.nextLine();

        String[] users = {"Abbas", "Leman", "Xedice", "Ilyas", "Nurlan", "Nihat", "Elchin", "Murad", "Mirhesen", "Emin", "Farid", "Terane"};

        int ifFounded = 0;
        for (String user: users){
            if (user.toLowerCase().startsWith(searchingValue)){
                System.out.println(user);
                ifFounded = 1;
            }
        }

        if (ifFounded==0){
            System.out.println("Belə istifadəçi yoxdur!");
        }

    }
}
