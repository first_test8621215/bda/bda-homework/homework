package Lesson5;
//7.Daxil edilən 2 ölçülü kvadrat matrisin baş diaqonal elementlərini çap edən proqram yazın. (int[][], for, şərt)
//8.Daxil edilmiş 2 ölçülü massivin sağ diaqonal elementlərini çap edən proqram yazın.
//9.Daxil edilmiş 2 ölçülü massivin sol diaqonal elementlərini tərsinə çevirən proqram tərtib edin.
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Collection.*;

public class DiagonalElementsExample {
    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int rows = matrix.length;
        int cols = matrix[0].length;

        int[] rightDiagonalElements = new int[Math.min(rows, cols)];
        int[] leftDiagonalElements = new int[Math.min(rows, cols)];
        int sum = 0;


        for (int i = 0; i < Math.min(rows, cols); i++) {
            leftDiagonalElements[i] = matrix[i][i];
        }

        for (int i = 0; i < Math.min(rows, cols); i++) {
            rightDiagonalElements[i] = matrix[i][rightDiagonalElements.length - 1 - i];
        }

        for (int i=0; i<leftDiagonalElements.length; i++){
            sum += leftDiagonalElements[i] + rightDiagonalElements[i];
        }
        System.out.println("Left Diagonal: " + Arrays.toString(leftDiagonalElements));
        System.out.println("Right Diagonal: " + Arrays.toString(rightDiagonalElements));

        List<Integer> reversedLeftDiagonal = Arrays.asList(Arrays.stream(leftDiagonalElements)
                .boxed()
                .toArray(Integer[]::new));
        Collections.reverse(reversedLeftDiagonal);
        System.out.println("Reverse Left Diagonal: " + reversedLeftDiagonal);

        List<Integer> reversedRightDiagonal = Arrays.asList(Arrays.stream(rightDiagonalElements)
                .boxed()
                .toArray(Integer[]::new));
        Collections.reverse(reversedRightDiagonal);
        System.out.println("Reverse Right Diagonal: " + reversedRightDiagonal);


        System.out.println(sum);
    }
}