package Lesson4;

//8)Rekursiyadan istifadə edərək factorial hesablayan
//proqram tərtib edin
//28).Fordan istifade etmeden Faktorial <- cox chetindir bu :) amma ugurlar
public class Factorial {
    public static void main(String[] args) {
        System.out.println(factorial(5));
    }

    public static int factorial(int number){
        if (number==0 || number==1){
            return 1;
        }else{
            return number * factorial(number-1);
        }
    }
}
