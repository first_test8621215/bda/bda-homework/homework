package Lesson4;
//6)Daxil edilən sətirdə hansı simvoldan neçə dəfə istifadə
//olunduğunu çap edən proqram yazın.

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RepeatedCharacters {
    public static void main(String[] args) {
        Map<String, Integer> myDict = new HashMap<String, Integer>();

        Scanner scan = new Scanner(System.in);
        System.out.print("Söz daxil edin: ");
        char[] charArrayOfInputValue = scan.nextLine().toCharArray();

        for (char c : charArrayOfInputValue) {
            if (myDict.containsKey(String.valueOf(c))) {
                myDict.put(String.valueOf(c), myDict.get(String.valueOf(c)) + 1);
            } else {
                myDict.put(String.valueOf(c), 1);
            }
        }
        System.out.println(myDict);

    }
}
