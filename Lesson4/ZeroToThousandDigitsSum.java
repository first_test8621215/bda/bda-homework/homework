package Lesson4;
//17). Write a Java program that reads an integer between 0 and 1000 and adds all
//the digits in the integer.
import java.util.Scanner;

public class ZeroToThousandDigitsSum {
    public static void main(String[] args) {
        Scanner valueScanner = new Scanner(System.in);
        System.out.print("Input Value: ");
        int value = valueScanner.nextInt();

        if (value < 1000 & value > 0){
            char[] digits = String.valueOf(value).toCharArray();
            int sum = 0;
            for (int i=0; i < digits.length ; i++){
                sum += Integer.parseInt(String.valueOf(digits[i]));
            }
            System.out.printf("The sum of all digits in %d is %d",value,sum);
        }else{
            System.out.println("Value is not between 0 and 1000!");
        }
    }
}
