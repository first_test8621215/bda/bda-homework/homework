package Lesson4;

import java.util.Scanner;

//19. Write a Java program that reads a number and display the square, cube, and
//fourth power.
public class SquareCubeFourthPower {
    public static void main(String[] args) {
        Scanner valueScanner = new Scanner(System.in);
        System.out.print("Input Value: ");
        int value = valueScanner.nextInt();

        StringBuilder sb = new StringBuilder(20);
        System.out.printf("Square       : %-10s\n", (int) Math.pow(value,2));
        System.out.printf("Cube         : %-10s\n", (int) Math.pow(value,3));
        System.out.printf("Fourth Power : %-10s\n", (int) Math.pow(value,4));

    }
}
