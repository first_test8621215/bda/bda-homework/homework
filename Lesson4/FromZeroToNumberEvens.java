package Lesson4;
//22)While Ve For ile 0-dan verilmish edede qeder butun 2-ye bolune bilen ededleri chap et. Ipucu: % ve / istifade edeceksiniz
import java.util.Scanner;

public class FromZeroToNumberEvens {
    public static void main(String[] args) {
        Scanner valueScanner = new Scanner(System.in);
        System.out.print("Input Value: ");
        int value = valueScanner.nextInt();

        for (int i=1; i<value; i++){
            if (i%2==0){
                System.out.println(i);
            }
        }
    }
}
