package Lesson4;
//21) Write a Java program to break an integer into a sequence of individual digits.
import java.util.Scanner;

public class NumberToİndividualDigits {
    public static void main(String[] args) {
        Scanner valueScanner = new Scanner(System.in);
        System.out.print("Input Value: ");
        int value = valueScanner.nextInt();

        char[] valueAsCharArray = String.valueOf(value).toCharArray();

        for(char onechar: valueAsCharArray){
            System.out.println(onechar);
        }
    }
}
