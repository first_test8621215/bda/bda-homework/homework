package Lesson4;
//34) 0-dan verilmish edede qeder butun 2-ye bolune bilen ededleri chap et. Ipucu: % ve / istifade edeceksiniz
//
//
//meselen: 5 daxil edirem. 0,2 ve 4 reqemleri 2-ye bolunur
//35)User enters a number and application must find and print all numbers which can be divided by 2 between zero and that given number.
import java.util.Scanner;

public class FromZeroToExactNumberIfIsDivided2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Söz daxil edin: ");
        int number = scan.nextInt();

        for(int i=1; i<number; i++){
            if (i%2==0){
                System.out.println(i);
            }
        }
    }
}
