package Lesson4;
//18. Write a Java program to convert minutes into a number of years and days.
import java.util.Scanner;

public class MinutesToYears {
    public static void main(String[] args) {
        Scanner minuteValueScan = new Scanner(System.in);
        System.out.print("Input Minute Value: ");
        double minuteValue = minuteValueScan.nextDouble();

        if (minuteValue>=525600){
            int yearValue = (int) minuteValue/525600;
            int dayvalue = (int) (minuteValue%525600)/1440;
            int hourValue = (int) ((minuteValue%525600)%1440)/60;
            double minuteValue2 = ((minuteValue%525600)%1440)%60;

            System.out.printf("%d minutes approximately is %s years and %s days and %s hours and %d minutes",(int) minuteValue,yearValue,dayvalue,hourValue,(int) minuteValue2);
        }else if(minuteValue>=1440 & minuteValue<525600){
            int dayvalue = (int) (minuteValue%525600)/1440;
            int hourValue = (int) ((minuteValue%525600)%1440)/60;
            double minuteValue2 = ((minuteValue%525600)%1440)%60;
            System.out.printf("%d minutes approximately is %s days and %s hours and %d minutes",(int) minuteValue,dayvalue,hourValue,(int) minuteValue2);
        }else if (minuteValue>=60 & minuteValue<1440){
            int hourValue = (int) minuteValue/60;
            double minuteValue2 = minuteValue%60;
            System.out.printf("%d minutes approximately is %s hours and %d minutes",(int) minuteValue,hourValue,(int) minuteValue2);
        }else{
            System.out.printf("%d minutes exactly is %d minutes",(int) minuteValue,(int) minuteValue);
        }

    }
}
