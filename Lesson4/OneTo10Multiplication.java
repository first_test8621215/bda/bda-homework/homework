package Lesson4;

import java.util.Scanner;

public class OneTo10Multiplication {
    public static void main(String[] args) {
        Scanner valueScan = new Scanner(System.in);
        System.out.print("Input Value: ");
        int value = valueScan.nextInt();

        for (int i=1 ; i<=10 ; i++){
            System.out.printf("%d * %d = %d\n",value,i,value*i);
        }

    }
}
