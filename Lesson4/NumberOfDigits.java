package Lesson4;
//27). Ededin reqemlerinin sayini tapin.//12345
import java.util.Scanner;

public class NumberOfDigits {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Ədəd daxil edin: ");
        int number = scan.nextInt();

        int numberLength = String.valueOf(number).length();

        System.out.println(numberLength);
    }
}
