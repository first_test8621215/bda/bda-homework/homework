package Lesson4;

import java.util.Scanner;

//33. 0-dan verilmish edede qeder sade ededleri chap et.
public class FromZeroToExactNumberPrimes {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Söz daxil edin: ");
        int number = scan.nextInt();

        for (int i = 1; i < number; i++) {
            if (SimpleOrComplex(i)){
                System.out.println(i);
            }
        }
    }

    public static boolean SimpleOrComplex(int argNumber) {
        if (argNumber <= 1) {
            return false;
        }
        if (argNumber == 2) {
            return true;
        }
        if (argNumber % 2 == 0) {
            return false;
        }
        for (int i = 3; i <= Math.sqrt(argNumber); i += 2) {
            if (argNumber % i == 0) {
                return false;
            }
        }
        return true;
    }
}