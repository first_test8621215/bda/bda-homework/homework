package Lesson4;
//16)Write a Java program that reads a number in inches, converts it to meters.
import java.util.Scanner;

public class InchToMeter {
    public static void main(String[] args) {
        Scanner inchValueScan = new Scanner(System.in);
        System.out.print("Input inch Value: ");
        int inchValue = inchValueScan.nextInt();
        System.out.println(inchToMeter(inchValue));
    }
    public static String inchToMeter(int inch){
        double m = inch * 0.0254;
        return String.format("%s inch equals to %s meter",String.valueOf(inch),String.valueOf(m));
    }
}
