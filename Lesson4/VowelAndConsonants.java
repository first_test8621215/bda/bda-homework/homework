package Lesson4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.HashSet;

// 7)Daxil edilmiş sətirdə neçə sait və neçə samit olduğunu
//tapan proqram tərtib edin.
import static java.util.Arrays.asList;

public class VowelAndConsonants {
    public static void main(String[] args) {
        String[] vowels = {"a","ı","o","u","e","ə","i","ö","ü"};
        String[] specailChars = {"'"," ","\"","`","(",")",".",",","<",">"};
        String[] numbers = {"0","1","2","3","4","5","6","7","8","9"};

        ArrayList<String> consonantsInWord = new ArrayList<>();
        ArrayList<String> vowelsInWord = new ArrayList<>();


        Scanner scan = new Scanner(System.in);
        System.out.print("Söz daxil edin: ");
        char[] charArrayOfInputValue = scan.nextLine().toLowerCase().toCharArray();

        for (char c: charArrayOfInputValue){
            if(Arrays.asList(vowels).contains(String.valueOf(c))){
                vowelsInWord.add(String.valueOf(c));
            }else if (Arrays.asList(specailChars).contains(String.valueOf(c))){
                continue;
            }else if (Arrays.asList(numbers).contains(String.valueOf(c))){
                continue;
            }
            else{
                consonantsInWord.add(String.valueOf(c));
            }
        }

        HashSet<String> consonantsOfWord = new HashSet<String>(consonantsInWord);
        HashSet<String> vowelsOfWord = new HashSet<String>(vowelsInWord);

        System.out.println("Sözdəki samitlər" + consonantsOfWord);
        System.out.println("Sözdəki saitlər" + vowelsOfWord);
    }
}
