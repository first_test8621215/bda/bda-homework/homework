package Lesson4;
//36) 0.Yazilmish reqemi soze chevirin meselen: 125 : bir yuz iyirmi besh

import java.util.HashMap;
import java.util.Scanner;

public class NumberToText {
    public static void main(String[] args) {
        HashMap<Integer, String> onluqlar = new HashMap<Integer, String>();
        onluqlar.put(2, "yüz");
        onluqlar.put(3, "min");
        onluqlar.put(4, "yüz min");
        onluqlar.put(5, "milyon");
        onluqlar.put(6, "yüz milyon");
        onluqlar.put(7, "milyard");
        onluqlar.put(8, "yüz milyard");
        onluqlar.put(9, "trilyon");

        HashMap<Integer, String> teklik = new HashMap<Integer, String>();
        teklik.put(0, "");
        teklik.put(1, "bir");
        teklik.put(2, "iki");
        teklik.put(3, "üç");
        teklik.put(4, "dörd");
        teklik.put(5, "beş");
        teklik.put(6, "altı");
        teklik.put(7, "yeddi");
        teklik.put(8, "səkkiz");
        teklik.put(9, "doqquz");

        HashMap<Integer, String> ikincionluq = new HashMap<Integer, String>();
        ikincionluq.put(0, "");
        ikincionluq.put(1, "on");
        ikincionluq.put(2, "iyirmi");
        ikincionluq.put(3, "otuz");
        ikincionluq.put(4, "qırx");
        ikincionluq.put(5, "əlli");
        ikincionluq.put(6, "altmış");
        ikincionluq.put(7, "yetmiş");
        ikincionluq.put(8, "səksən");
        ikincionluq.put(9, "doxsan");

        Scanner scan = new Scanner(System.in);
        System.out.print("Söz daxil edin: ");
        int number = scan.nextInt();
        StringBuilder numberAsString = new StringBuilder();
        numberAsString.append(number).reverse();

        StringBuilder resultString = new StringBuilder();
        for (int i=numberAsString.length()-1; i>=0; i--){
            if (i==0){
                resultString.append(teklik.get(Integer.parseInt(String.valueOf(numberAsString.charAt(0))))).append(" ");
            }else if (i==1){
                resultString.append(ikincionluq.get(Integer.parseInt(String.valueOf(numberAsString.charAt(1))))).append(" ");
            }else if(i>=2){
                String first = teklik.get(Integer.parseInt(String.valueOf(numberAsString.charAt(i))));
                String second = onluqlar.get(i);
                resultString.append(first).append(" ").append(second).append(" ");
            }
        }
        System.out.println(resultString);
    }
}
