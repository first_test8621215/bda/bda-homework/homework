package Lesson4;
//10) Write a Java program to divide two numbers and print on the screen.
//Test Data :

import java.util.Arrays;
import java.util.Scanner;

public class Division {
    public static void main(String[] args) {
        Scanner operationScan = new Scanner(System.in);
        String operation = operationScan.nextLine();
        String[] values = operation.split("/");
        System.out.println(Integer.parseInt(values[0])/Integer.parseInt(values[1]));
    }
}
