package Lesson4;
//23) For dovrunden istifade ederek VurmaCedveli yaratmaq.
public class MultiplicationTable {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            for (int i2 = 1; i2 <= 10; i2++) {
                System.out.println(i + " x " + i2 + " = " + i*i2);
            }
            System.out.println("=".repeat(20));
        }
    }
}