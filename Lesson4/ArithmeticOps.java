package Lesson4;
//13)Write a Java program to print the sum (addition), multiply, subtract, divide and
//        remainder of two numbers
import java.util.Scanner;

public class ArithmeticOps {
    public static void main(String[] args) {
        Scanner firstValueScan = new Scanner(System.in);
        Scanner secondValueScan = new Scanner(System.in);

        System.out.print("Input First Value: ");
        float firstValue = firstValueScan.nextFloat();
        System.out.print("Input Second Value: ");
        float secondValue = secondValueScan.nextFloat();

        System.out.printf("%.1f + %.1f = %.1f%n", firstValue, secondValue, firstValue + secondValue);
        System.out.printf("%.1f - %.1f = %.1f%n", firstValue, secondValue, firstValue - secondValue);
        System.out.printf("%.1f * %.1f = %.1f%n", firstValue, secondValue, firstValue * secondValue);
        System.out.printf("%.1f / %.1f = %.1f%n", firstValue, secondValue, firstValue / secondValue);


    }
}
