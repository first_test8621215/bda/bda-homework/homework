package Lesson4;

import java.util.Scanner;

//20). Write a Java program that accepts two integers from the user and then prints
//the sum, the difference, the product, the average, the distance
public class TwoNumberOps {
    public static void main(String[] args) {
        Scanner firstValueScan = new Scanner(System.in);
        Scanner secondValueScan = new Scanner(System.in);

        System.out.print("Input First Value: ");
        int firstValue = firstValueScan.nextInt();
        System.out.print("Input Second Value: ");
        int secondValue = secondValueScan.nextInt();

        System.out.printf("%d + %d = %d\n",firstValue,secondValue,firstValue+secondValue);
        System.out.printf("%d - %d = %d\n",firstValue,secondValue,firstValue-secondValue);
        System.out.printf("%d and %d average = %d\n",firstValue,secondValue,(firstValue+secondValue)/2);
        System.out.printf("%d x %d = %d\n",firstValue,secondValue,firstValue*secondValue);


    }

}
