package Lesson4;
//1)Daxil edilmiş ədədə qədər bütün cüt ədədləri çap edən
//proqram yazın.
//2)Daxil edilmiş ədədə qədər bütün tək ədədləri çap edən
//proqram yazın.

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class OddsAndEvensUntil {
    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(0);
        arr.add(1);
        arr.add(2);
        arr.add(3);
        arr.add(4);
        arr.add(5);
        arr.add(6);
        arr.add(7);
        arr.add(8);
        arr.add(9);

        Scanner scan = new Scanner(System.in);
        System.out.print("Hara kimi baxılsın?: ");
        int number = scan.nextInt();

        ArrayList<Integer> oddNumbers = new ArrayList<>();
        ArrayList<Integer> evenNumbers = new ArrayList<>();

        if (arr.contains(number)) {
            int index_num = arr.indexOf(number);
            List<Integer> tempArr = arr.subList(0, index_num);
            for (int i = 0; i < tempArr.size(); i++) {
                if (tempArr.get(i) % 2 == 0 & tempArr.get(i) != 0) {
                    evenNumbers.add(tempArr.get(i));
                } else if (tempArr.get(i) % 2 == 1) {
                    oddNumbers.add(tempArr.get(i));
                } else {
                    System.out.println("Ədəd sıfırdır.");
                }
            }
        }
        System.out.println(oddNumbers);
        System.out.println(evenNumbers);
    }
}
