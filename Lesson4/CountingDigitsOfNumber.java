package Lesson4;

//3)Daxil edilən ədədin bütün rəqəmlərinin cəmini tapan
//proqram yazın.
//4)Daxil edilən ədədin sadə yoxsa mürəkkəb olduğunu
//tapan proqram yazın. (Scanner, for, şərt)
//5)Daxil edilmiş sətirin tərsi ilə düzünün eyni olub
//        olmadığını yoxlayan proqram yazın.



import java.util.ArrayList;
import java.util.Scanner;

public class CountingDigitsOfNumber {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Ədəd daxil edin: ");
        int number = scan.nextInt();
        System.out.println(SimpleOrComplex(number));
        char[] charArray = String.valueOf(number).toCharArray();

        int result = 0;
        for (int i = 0; i < charArray.length; i++) {
            result += Integer.parseInt(String.valueOf(charArray[i]));
        }
        System.out.println(result);

        StringBuilder reversedString = new StringBuilder();
        for (int j = charArray.length-1 ; j >= 0; j-- ){
            reversedString.append(charArray[j]);
        }
        System.out.println("Ədədin tərsi: " + reversedString);
    }

    public static String SimpleOrComplex(int argNumber) {
        if (argNumber <= 1) {
            return "1 və ondan kiçik ədədlər sadə deyil!";
        }
        if (argNumber == 2) {
            return "2 sadə ədəddir!";
        }
        if (argNumber % 2 == 0) {
            return argNumber + " mürəkkəb ədəddir!";
        }
        for (int i = 3; i <= Math.sqrt(argNumber); i += 2) {
            if (argNumber % i == 0) {
                return argNumber + " mürəkkəb ədəddir!";
            }
        }
        return argNumber + " sadə ədəddir!";
    }
}

