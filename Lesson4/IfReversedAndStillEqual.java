package Lesson4;

import java.util.Scanner;

//29).Verilen string-in tersi ile duzunun bir birine beraber olub olmadigi.
//30.Daxil edilen reqemlerin tersi ile duzunun eyni olub olmadigini teyin eden method yazin
public class IfReversedAndStillEqual {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Söz daxil edin: ");
        String word = scan.nextLine();

        StringBuilder reversedNumber = new StringBuilder();
        reversedNumber.append(word).reverse();
        Boolean reversedEqualsToStill = word.contentEquals(reversedNumber);
        System.out.println(reversedEqualsToStill);
    }
}
