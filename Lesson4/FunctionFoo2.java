package Lesson4;
//32.method 3 reqem qebul edir a,b,c,
//public static void foo(int a, int b, int c)
//a-dan b-ye qeder butun ededleri c qeder quvvete yukseldir Math.pow-dan istifadə edərək
//foo(1,5,2) -> 1^2=1; 2^2=4; 3^2=9, 4^2=16, 5^2=25
public class FunctionFoo2 {
    public static void main(String[] args) {
        foo(1,10,3);
    }
    public static void foo(int a, int b, int c){
        for(;a<b; a++){
            System.out.println(a + " ^ " + c + " = " + Math.pow(a,c));
        }
    }
}
