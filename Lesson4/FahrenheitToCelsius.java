package Lesson4;
//15)Write a Java program to convert temperature from Fahrenheit to Celsius degree.
import java.util.Scanner;

public class FahrenheitToCelsius {
    public static void main(String[] args) {
        Scanner valueScan = new Scanner(System.in);
        System.out.print("Fahrenheit : ");
        int value = valueScan.nextInt();

        System.out.println(fahrenheitToCelsius(value));

    }
    public static String fahrenheitToCelsius(double fahrenheit) {
        double celsius = (fahrenheit - 32) * 5/9;
        return String.format("%s Fahrenheit equals to %s Celsius",String.valueOf(fahrenheit),String.valueOf(celsius));
    }
}

