package Lesson4;
//11) Write a Java program to print the result of the following operations.
//Test Data:
//a. -5 + 8 * 6
//b. (55+9) % 9
//c. 20 + -3*5 / 8
//d. 5 + 15 / 3 * 2 - 8 % 3

//10) Write a Java program to divide two numbers and print on the screen.
//12). Write a Java program that takes two numbers as input and display the product
//of two numbers.

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator3 {
    public static void main(String[] args) {
        String[] operands = {"(", "*", "/", "%", "+", "-"};

        Scanner operationScan = new Scanner(System.in);
        String operation = operationScan.nextLine();

        boolean containsElement = false;

        while (true) {
            for (String operand : operands) {
                if (operation.contains(operand)) {
                    containsElement = true;
                    break;
                }
            }

            if (containsElement) {

                for (String operand : operands) {
                    if (operation.contains(operand)) {
                        if (operand.equals("(")) {
                            operation = Brackets(operation);
                        } else if (operand.equals("*")) {
                            operation = Multiplication(operation);
                        } else if (operand.equals("/")) {
                            operation = Division(operation);
                        } else if (operand.equals("%")) {
                            operation = Modulo(operation);
                        } else if (operand.equals("+")) {
                            operation = Addition(operation);
                        } else if (operand.equals("-")) {
                            operation = Subtraction(operation);
                        } else {
                            System.out.println("Incorrect operand !");
                            System.exit(0);
                        }
                    }
                }
                containsElement = false;
            }else {
                break;
            }
        }
        System.out.println(operation);
    }

    public static String Brackets(String suboperation) {
        String[] operands = {"(", "*", "/", "+", "-"};

        Pattern pattern = Pattern.compile("\\((.*?)\\)");
        Matcher matcher = pattern.matcher(suboperation);
        while (matcher.find()) {
            String match = matcher.group(1);
            int StartIndex = suboperation.indexOf(match);
            int FinishIndex = StartIndex + match.length();

            for (String operand2 : operands) {
                if (match.contains(operand2)) {
                    if (operand2.equals("(")) {
                        match = Brackets(match);
                    } else if (operand2.equals("*")) {
                        match = Multiplication(match);
                    } else if (operand2.equals("/")) {
                        match = Division(match);
                    } else if (operand2.equals("%")) {
                        match = Modulo(match);
                    } else if (operand2.equals("+")) {
                        match = Addition(match);
                    } else if (operand2.equals("-")) {
                        match = Subtraction(match);
                    }
                }
            }
            String result = String.valueOf(match);
            suboperation = suboperation.replace(suboperation.substring(StartIndex - 1, FinishIndex + 1), String.valueOf(result));

            return suboperation;
        }
        return "";
    }

    public static String Multiplication(String suboperation) {
        String pattern2 = "(\\(?-?\\d+\\.?\\d*\\)?)\\s*\\*\\s*(\\(?-?\\d+\\.?\\d*\\)?)";


        Pattern r = Pattern.compile(pattern2);
        Matcher m = r.matcher(suboperation);

        if (m.find()) {
            String a = "";
            String b = "";
            float a_int = 0;
            float b_int = 0;

            if (m.group(2).contains("(")) {
                b = Brackets(m.group(2));
            } else if (m.group(1).contains("(")) {
                a = Brackets(m.group(1));
            } else {
                a = m.group(1);
                b = m.group(2);
            }

            if (!a.isEmpty()) {
                b = m.group(2);
            } else if (!b.isEmpty()) {
                a = m.group(1);
            }

            a_int = Float.parseFloat(a);
            b_int = Float.parseFloat(b);
            String wholeMatch = m.group(0);

            float result = a_int * b_int;

            int StartIndex = suboperation.indexOf(wholeMatch);
            int FinishIndex = StartIndex + wholeMatch.length();

            suboperation = suboperation.replace(suboperation.substring(StartIndex, FinishIndex), String.valueOf(result));

            return suboperation;

        }
        return "";
    }

    public static String Division(String suboperation) {
        String pattern2 = "(\\(?-?\\d+\\.?\\d*\\)?)\\s*/\\s*(\\(?-?\\d+\\.?\\d*\\)?)";

        Pattern r = Pattern.compile(pattern2);
        Matcher m = r.matcher(suboperation);

        if (m.find()) {
            String a = "";
            String b = "";
            float a_int = 0;
            float b_int = 0;

            if (m.group(2).contains("(")) {
                b = Brackets(m.group(2));
            } else if (m.group(1).contains("(")) {
                a = Brackets(m.group(1));
            } else {
                a = m.group(1);
                b = m.group(2);
            }
            if (!a.isEmpty()) {
                b = m.group(2);
            } else if (!b.isEmpty()) {
                a = m.group(1);
            }

            a_int = Float.parseFloat(a);
            b_int = Float.parseFloat(b);


            String wholeMatch = m.group(0);

            String resultString = "";
            float result = a_int / b_int;

            if (result < 0) {
                resultString = "(" + String.valueOf(result) + ")";
            } else {
                resultString = String.valueOf(result);
            }

            resultString = String.valueOf(result);


            int StartIndex = suboperation.indexOf(wholeMatch);
            int FinishIndex = StartIndex + wholeMatch.length();

            suboperation = suboperation.replace(suboperation.substring(StartIndex, FinishIndex), resultString);

            return suboperation;

        }
        return "";

    }

    public static String Modulo(String suboperation) {
        String pattern2 = "(\\(?-?\\d+\\.?\\d*\\)?)\\s*%\\s*(\\(?-?\\d+\\.?\\d*\\)?)";

        Pattern r = Pattern.compile(pattern2);
        Matcher m = r.matcher(suboperation);

        if (m.find()) {
            String a = "";
            String b = "";
            float a_int = 0;
            float b_int = 0;

            if (m.group(2).contains("(")) {
                b = Brackets(m.group(2));
            } else if (m.group(1).contains("(")) {
                a = Brackets(m.group(1));
            } else {
                a = m.group(1);
                b = m.group(2);
            }

            if (!a.isEmpty()) {
                b = m.group(2);
            } else if (!b.isEmpty()) {
                a = m.group(1);
            }

            a_int = Float.parseFloat(a);
            b_int = Float.parseFloat(b);

            String wholeMatch = m.group(0);

            float result = a_int % b_int;

            int StartIndex = suboperation.indexOf(wholeMatch);
            int FinishIndex = StartIndex + wholeMatch.length();

            suboperation = suboperation.replace(suboperation.substring(StartIndex, FinishIndex), String.valueOf(result));

            return suboperation;

        }
        return "";
    }


    public static String Addition(String suboperation) {
        String pattern2 = "(\\(?-?\\d+\\.?\\d*\\)?)\\s*\\+\\s*(\\(?-?\\d+\\.?\\d*\\)?)";

        Pattern r = Pattern.compile(pattern2);
        Matcher m = r.matcher(suboperation);

        if (m.find()) {
            String a = "";
            String b = "";
            float a_int = 0;
            float b_int = 0;

            if (m.group(2).contains("(")) {
                b = Brackets(m.group(2));
            } else if (m.group(1).contains("(")) {
                a = Brackets(m.group(1));
            } else {
                a = m.group(1);
                b = m.group(2);
            }

            if (!a.isEmpty()) {
                b = m.group(2);
            } else if (!b.isEmpty()) {
                a = m.group(1);
            }

            a_int = Float.parseFloat(a);
            b_int = Float.parseFloat(b);

            String wholeMatch = m.group(0);

            float result = a_int + b_int;

            int StartIndex = suboperation.indexOf(wholeMatch);
            int FinishIndex = StartIndex + wholeMatch.length();

            suboperation = suboperation.replace(suboperation.substring(StartIndex, FinishIndex), String.valueOf(result));

            return suboperation;

        }
        return "";
    }

    public static String Subtraction(String suboperation) {
        String pattern1 = "(\\(?-\\d+\\.?\\d*\\)?)\\s*-\\s*(\\(-\\d+\\.?\\d*\\)?)";
        String pattern2 = "(\\d+\\.?\\d*)\\s*-\\s*(\\(?-?\\d+\\.?\\d*\\)?)";
        String patternForMinus = "-(\\d+\\.?\\d*)";

        Pattern r0 = Pattern.compile(pattern1);
        Matcher m0 = r0.matcher(suboperation);

        Pattern r = Pattern.compile(pattern2);
        Matcher m = r.matcher(suboperation);

        Pattern r2 = Pattern.compile(patternForMinus);
        Matcher m2 = r2.matcher(suboperation);

        if (m0.find()) {
            String a = "";
            String b = "";
            float a_int = 0;
            float b_int = 0;

            if (m.group(2).contains("(")) {
                b = Brackets(m.group(2));
            } else if (m.group(1).contains("(")) {
                a = Brackets(m.group(1));
            } else {
                a = m.group(1);
                b = m.group(2);
            }

            if (!a.isEmpty()) {
                b = m.group(2);
            } else if (!b.isEmpty()) {
                a = m.group(1);
            }

            a_int = Float.parseFloat(a);
            b_int = Float.parseFloat(b);

            String wholeMatch = m.group(0);

            float result = a_int - b_int;

            int StartIndex = suboperation.indexOf(wholeMatch);
            int FinishIndex = StartIndex + wholeMatch.length();

            suboperation = suboperation.replace(suboperation.substring(StartIndex, FinishIndex), String.valueOf(result));

            return suboperation;

        } else if (m.find()) {
            String a = "";
            String b = "";
            float a_int = 0;
            float b_int = 0;

            if (m.group(2).contains("(")) {
                b = Brackets(m.group(2));
            } else if (m.group(1).contains("(")) {
                a = Brackets(m.group(1));
            } else {
                a = m.group(1);
                b = m.group(2);
            }

            if (!a.isEmpty()) {
                b = m.group(2);
            } else if (!b.isEmpty()) {
                a = m.group(1);
            }

            a_int = Float.parseFloat(a);
            b_int = Float.parseFloat(b);

            String wholeMatch = m.group(0);

            float result = a_int - b_int;

            int StartIndex = suboperation.indexOf(wholeMatch);
            int FinishIndex = StartIndex + wholeMatch.length();

            suboperation = suboperation.replace(suboperation.substring(StartIndex, FinishIndex), String.valueOf(result));

            return suboperation;

        } else if (m2.find()) {
            return m2.group(0).replace("(", "").replace(")", "");
        }
        return "";
    }
}
