package Lesson4;
//24)Verilmish ededin butun ededleri cemini tap.12345 - 1+2+3+4+5
import java.util.Scanner;

public class SumDIgits {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Ədəd daxil edin: ");
        int number = scan.nextInt();
        char[] charArray = String.valueOf(number).toCharArray();

        long sum = 0;
        for (char onechar: charArray){
            sum += Integer.valueOf(String.valueOf(onechar));
        }

        System.out.println(sum);
    }
}
