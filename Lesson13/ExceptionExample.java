package Lesson13;

public class ExceptionExample {
    public static void main(String[] args) {
        System.out.println(Exceptions());
    }

    public static String Exceptions() {
        String result = "";
        String v = null;
        try {
            try {
                result += "before ";
                v.length();
                result += "after ";
            } catch (NullPointerException e) {
                System.out.println(e.getMessage());
                result += "catch ";
//                throw new RuntimeException();
                return result;
            } finally {
                result += "finally ";
                throw new Exception();
            }
        } catch (Exception e) {
            result += "done ";
        }
        return result;
    }
}

