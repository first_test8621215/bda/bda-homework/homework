package Lesson7;

public class Shark extends Fish{
    private int number = 8;

    public Shark(int age, int size) {
        super(age);
        this.size = size;
    }
    public void displaySharkDetails(){
        System.out.println("Shark age: " + this.age);
        System.out.println("Shark size: " + size);
        System.out.println("Shark number: " + number);
    }

    public static void main(String[] args) {
        new Shark(1,6).displaySharkDetails();
    }
}
