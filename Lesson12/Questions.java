package Lesson12;

public class Questions {
    public static void main(String[] args) {
       String a = new String();
       String b = null;
       System.out.println(null==null);
    }
}

enum question1 {
        //ONE, TWO, THREE, ONE, FOUR;
}
// Variable 'ONE' is already defined in the scope

enum question2{ //extends Questions {
    ABC, BCD, CDE, DEF;
}
// Enum can not extend from class.

enum question3{
//    private TOP,
//
//    public  MEDIUM,
//
//    protected BOTTOM;
}

// Enum constants must be declared without any access modifiers because they are themselves always public, static, and final.

enum question4{
    A, B, C;

    question4()
    {
        System.out.println(1);
    }

}

// question4() is constructor of question4 enum and when we call each instance of enum , constructor will be invoked before the value of instance itself.

//5) Enum types can have public constructors. True OR False?
// FALSE

enum question6
{
    A, B, C;


    static
    {
        System.out.println(2);
    }

    {
        System.out.println(1);
    }


    private question6()
    {
        System.out.println(3);
    }
}
// Not found !

enum question7
{
    NORTH, SOUTH, WEST, EAST;

    private question7()
    {
        System.out.println(1);
    }
}
//7) Enum class can not be initiated.


//8) Can enum types implement interfaces?
// Yes, they can.

enum question8
{
    FIRST, SECOND, THIRD, FOURTH;
}

// All of them nulls.
