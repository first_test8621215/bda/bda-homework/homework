package Lesson15;

public class GenericsMethods {
    public static <T> boolean isEqual(GenericsType<T> g1, GenericsType<T> g2){
        return g1.get() == g2.get();
    }

    public static void main(String args[]){
        GenericsType<Integer> g1= new GenericsType<>();
        g1.set(3);

        GenericsType<Integer> g2= new GenericsType<>();
        g2.set(3);

        boolean isEqual = GenericsMethods.isEqual(g1,g2);
        System.out.println(isEqual);
    }

}
