package Lesson2;

import java.util.Scanner;

//8). Iki tipde eded sechin ve bunlari boldukde cavab kesir
//ile alinsin.
public class Division {
    public static void main(String[] args) {
        Scanner ilk_eded_scanner = new Scanner(System.in);
        Scanner ikinci_eded_scanner = new Scanner(System.in);

        System.out.print("İlk ədədi daxil edin: ");
        int ilk_eded = Integer.parseInt(ilk_eded_scanner.next());
        System.out.print("Ikinci ədədi daxil edin: ");
        int ikinci_eded = Integer.parseInt(ikinci_eded_scanner.next());
        System.out.println("Nəticə: " + (float) ilk_eded /(float) ikinci_eded);
    }
}
