package Lesson2;

//1)Write a Java program to get the character at the given index within the
//String
public class indexOfString {
    public static void main(String[] args) {
        String a = args[0]; // String
        String b = args[1]; // index as string
        int c = Integer.parseInt(b); // convert index from string to int
        System.out.println(a.charAt(c)); // print to console.
    }
}
