package Lesson2;

import java.util.Scanner;

public class ArithmeticOpsOfTwoDigits {
    public static void main(String[] args) {
        Scanner a= new Scanner(System.in);
        Scanner b= new Scanner(System.in);
        Scanner c= new Scanner(System.in);


        System.out.print("First Value: ");
        int a_value = Integer.parseInt(a.nextLine());
        System.out.print("Second value: ");
        int b_value = Integer.parseInt(b.nextLine());

        System.out.print("""
                Choose the operation:
                1) +
                2) -
                3) *
                4) /
                5) Just Print Them
                6) Exit       
                """);

        // Break -lə dövrdən çıxmır.
        while(true) {
            String c_value = c.nextLine();
            switch (c_value) {
                case ("1"):
                    System.out.println(a_value + b_value);
                case ("2"):
                    System.out.println(a_value - b_value);
                case ("3"):
                    System.out.println(a_value * b_value);
                case ("4"):
                    System.out.println((float) a_value / (float) b_value);
                case ("5"):
                    System.out.println(a_value + " " + b_value);
                case ("6"):
                    System.exit(0);

                default:
                    System.out.println("There is no such option, try again");
            }
            break;
        }
    }
}
