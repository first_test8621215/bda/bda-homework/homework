package Lesson2;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

//17) 2 soz daxil edilir uzunlugu boyuk olan String geriye return edilir.
public class longestStingReturn {
    public static void main(String[] args) {
        Scanner first_scan = new Scanner(System.in);
        Scanner second_scan = new Scanner(System.in);

        System.out.print("İlk sözü daxil edin: ");
        String first_value = first_scan.next();
        System.out.print("İkinci sözü daxil edin: ");
        String second_value = second_scan.next();

        String[] list_of_values = {first_value,second_value};
        int longest_len = Arrays.stream(list_of_values).collect(Collectors.summarizingInt(String::length)).getMax();

        for(int i=0; i < list_of_values.length;i++){
            if (list_of_values[i].length() == longest_len){
                System.out.println(list_of_values[i]);
            }
        }
    }
}
