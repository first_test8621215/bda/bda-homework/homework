package Lesson2;

import java.util.Scanner;


// 9) Bir byte tipini int tipine chevirin.

public class byteToInt {
    public static void main(String[] args) {
        Scanner byte_value_scan = new Scanner(System.in);
        System.out.println("Nə isə daxil edin: ");
        String byte_value_as_string = byte_value_scan.next();
        byte[] byte_value = byte_value_as_string.getBytes();
        System.out.println("Byte value: ");
        System.out.println(byte_value);
        System.out.println("Byte value as integer: ");
        for (int i=0; i < byte_value.length; i++) {
            System.out.println(byte_value[i]);
        }

    }
}
