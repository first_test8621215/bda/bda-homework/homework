package Lesson8;
//Single Inheritance nümunə proqram yazın.
public class SingleInheritance extends SuperClass {
    public SingleInheritance(int a) {
        super(a);
        b = super.b;
    }

    public static void main(String[] args) {
        SingleInheritance newObject = new SingleInheritance(3);
        SuperClass newObject2 = new SuperClass(1);
        newObject.b = 3;
        System.out.println(newObject.b);
        System.out.println(newObject2.b);
    }
}

class SuperClass{
    int a;
    static int b=2;

    public SuperClass(int a) {
        this.a = a;
    }
}
