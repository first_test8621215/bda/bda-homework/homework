package Lesson8;
//Multilevel İnheritance nümunə proqram yazın.
public class MultilevelInheritance {
    public static void main(String[] args) {
        University newObject = new University();
        newObject.studyPrint();
        newObject.kindergardenPrint();
        newObject.schoolPrint();
        newObject.universityPrint();
    }
}

class Study{
    public void studyPrint(){
        System.out.println("Studying is fun!");
    }
}

class Kindergarden extends Study{
    public void kindergardenPrint(){
        System.out.println("Kindergarten is kinda fun!");
    }
}

class School extends Kindergarden{
    public void schoolPrint(){
        System.out.println("School is boring enough!");
    }
}

class University extends School{
    public void universityPrint(){
        System.out.println("University is so-so!");
    }
}